//how to execute: node sign.js <path file> <path private key>
//output: signature of file
var crypto = require('crypto');
var fs = require('fs');


//openssl genrsa -out key.pem 1024
var privatePem = fs.readFileSync('key.pem');
var key = privatePem.toString();

var sign = crypto.createSign('RSA-SHA256');

var buffer = fs.readFileSync('sample.txt');

sign.update(buffer);

var sig = sign.sign(key, 'hex');

console.log(sig);
