export default {
  buildModules: ['@nuxt/typescript-build'],
  buildDir: 'build',
  build: {
    publicPath: 'http://35.242.252.249:9000/system/build/dist/client/'
  }
}
