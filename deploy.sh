MINIO_ACCESS_KEY=samplekey
MINIO_SECRET_KEY=samplesecretkey
MINIO_ADDRESS=http://127.0.0.1:9001

KB_NS_NAME=spinnaker
KB_SERVICE_ACCOUNT_NAME=service-ac-v2
HAL_ACCOUNT_NAME=hal-account-v2

hal config provider kubernetes enable

kubectl create ns ${KB_NS_NAME}

kubectl create serviceaccount ${KB_SERVICE_ACCOUNT_NAME} -n ${KB_NS_NAME}

kubectl create clusterrolebinding ${KB_SERVICE_ACCOUNT_NAME} --clusterrole cluster-admin --serviceaccount=spinnaker:${KB_SERVICE_ACCOUNT_NAME}

TOKEN_SECRET=$(kubectl get serviceaccount -n ${KB_NS_NAME} ${KB_SERVICE_ACCOUNT_NAME} -o jsonpath='{.secrets[0].name}')

TOKEN=$(kubectl get secret -n ${KB_NS_NAME} $TOKEN_SECRET -o jsonpath='{.data.token}' | base64 --decode)

kubectl config set-credentials spinnaker-token-user --token $TOKEN

#kubectl config set-context --current --user spinnaker-token-user

CONTEXT=$(kubectl config current-context)

hal config provider kubernetes account add ${HAL_ACCOUNT_NAME} --provider-version v2 --context ${CONTEXT}

hal config deploy edit --type distributed --account-name ${HAL_ACCOUNT_NAME}

hal config features edit --artifacts true

hal config storage s3 edit --access-key-id ${MINIO_ACCESS_KEY} --secret-access-key ${MINIO_SECRET_KEY} --endpoint ${MINIO_ADDRESS}

hal config storage edit --type s3

#hal config security api edit --override-base-url http://spinnaker-api.storadewebservices.com

#hal config security ui edit --override-base-url http://spinnaker.storadewebservices.com

hal config version edit --version 1.18.4

hal deploy apply
